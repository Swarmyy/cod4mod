**P90**: damage 15, minDamage 10, startAmmo 150, maxAmmo 300, maxDamageRange 375, minDamageRange 500

**P90 Silencer**: maxDamageRange 175, minDamageRange 350


**USP**: damage 20, minDamage 10, raiseTime 0.45, clipSize 15


**RPD**: damage 20, minDamage 15, clipSize 70

**RPG**: explosionRadius 200, explosionInnerDamage 80, explosionOuterDamage 15, swayVertScale 0.4, adsSwayVertScale 0.4, adsSwayPitchScale 0.4, adsSwayYawScale 0.4

**RPG reflex**: adsSwayVertScale 1

**RPG ACOG**: adsSwayVertScale 0.08


**SAW**: damage 15, minDamage 10, swayVertScale 0.4, adsSwayVertScale 2, adsSwayPitchScale 0.4, adsSwayYawScale 0.4

**SAW ACOG**: adsSwayVertScale 0.04

**SAW Bipod**: damage 20, minDamage 15, maxDamageRange 200, minDamageRange 600, fireTime 0.1


**UZI**: damage 15, minDamage 10

**UZI ACOG**: fireType Single Shot, fireTime 0.252, damage 50, minDamage 30, maxDamageRange 2250, minDamageRange 3000, hipGunKickPitchMin 70, hipGunKickPitchMax, 80 hipGunKickYawMin -50, hipGunKickYawMax 50, 


**W1200**: maxAmmo 50, startAmmo 25, clipSize 5, shotCount 7, damage 20, minDamage 10, maxDamageRange 200, minDamageRange 1000


**Skorpion**: damage 15, minDamage 10, clipSize 60, maxAmmo 240, startAmmo 150, dropAmmoMax 60, minDamageRange 600, raiseTime 0.6, quickRaiseTime 0.35, firstRaiseTime 0.6, emptyRaiseTime 0.6


**Skorpion silencer**: minDamageRange 500


**AK47**: maxAmmo 150, damage 20, minDamage 15, maxDamageRange 1300, minDamageRange 1800, locHelmet 4, locHead 4, locTorsoLower 1.25, locRightLegUpper 1.25, locRightLegLower 0.74, locRightFoot 0.74, locLeftLegUpper 1.25, locLeftLegLower 0.74, locLeftFoot 0.74, fireTime 0.1, reloadTime 2.4, reloadEmptyTime 2.4, raiseTime 0.98, firstRaiseTime 0.98, emptyRaiseTime 0.98, swayLerpSpeed 1, swayHorizScale 1.2, swayVertScale 0.8, adsSwayLerpSpeed 2, adsSwayHorizScale 0.56, adsSwayVertScale 2, hipSpreadMax 14, hipSpreadDuckedMax 12, hipSpreadProneMax 10, hipSpreadMoveAdd 10

**AK47 Silencer**: maxDamageRange 430, minDamageRange 600

**AK47 ACOG**: swayLerpSpeed 2, swayMaxAngle 40, adsSwayLerpSpeed 4, adsSwayMaxAngle 4, adsSwayHorizScale 0.08, adsSwayVertScale 0.16


**AK74u**: maxAmmo 100, startAmmo 75, clipSize 25, dropAmmoMax 25, damage 20, minDamage 10, maxDamageRange 1000, minDamageRange 3000, fireTime 0.1, adsMoveSpeedScale 0.76, reloadEmptyTime 2.5, locHelmet 4, locHead 4, locRightLegUpper 0.85, locRightLegLower 0.85, locRightFoot 0.85, locLeftLegUpper 0.85, locLeftLegLower 0.85, locLeftFoot 0.85, raiseTime 1, hipSpreadStandMin 2.5, hipSpreadDuckedMin 2.1, hipSpreadProneMin 2.1, hipSpreadMax 10, hipSpreadDuckedMax 8.5, hipSpreadProneMax 8.5, hipSpreadFireAdd 0.6, hipSpreadMoveAdd 20, hipSpreadDecayRate 2.67, hipSpreadDuckedDecay 2.42, hipSpreadProneDecay 2.42

**AK74u Silencer**: maxDamageRange 500, minDamageRange 1500


**C4**: explosionInnerDamage 100, explosionOuterDamage 25


**Claymore**: explosionInnerDamage 100, explosionOuterDamage 25


**Cobra 20mm**: damage 5, minDamage 5, fireTime 0.1


**Cobra ffar**: damage 150, minDamage 15, fireTime 0.16


**Concussion**: damage 15, fireDelay 0.15, fireTime 0.7, fuseTime 1.5, aiFuseTime 2, holdFireTime 0.6, maxAmmo 6, startAmmo 2, clipSize 6, sharedAmmoCap 6


**Flash**: maxAmmo 6, startAmmo 2, clipSize 6, sharedAmmoCap 6


**Smoke**: maxAmmo 6, startAmmo 2, clipSize 6, sharedAmmoCap 6


**Hind ffar**: damage 150, minDamage 15, fireTime 0.16


**Humvee 50cal**: damage 5, minDamage 5, fireTime 0.2


**Colt 45**: damage 20, minDamage 10


**Frag Grenade**: explosionInnerDamage 150, explosionOuterDamage 40


**G3**: damage 25, minDamage 20 // damage 20, minDamage 15

**gl G3**: impactType bullet_large, maxAmmo 30, startAmmo 30, clipSize 30, damage 20, fireTime 0.17, explosionRadius 50, explosionInnerDamage 15, explosionOuterDamage 10, gunMaxPitch 8, gunMaxYaw 25, hipGunKickPitchMin 5, hipGunKickPitchMax -15, hipGunKickYawMin 5, hipGunKickYawMax -5, hipGunKickAccel 800, hipGunKickSpeedMax 2000, hipGunKickSpeedDecay 16, hipGunKickStaticDecay 20, hipViewKickPitchMin -30, hipViewKickPitchMax 60, hipViewKickYawMin 60, hipViewKickYawMax -60, fireRumble assault_fire


**G36C**: damage 15, minDamage 10, fireTime 0.09, locHelmet 2, locHead 2

**G36C Base**: clipSize 15, dropAmmoMax 15, damage 30, minDamage 20, maxDamageRange 750, minDamageRange 1000, fireTime 0.28

**G36C Reflex**: clipSize 35, dropAmmoMax 35, maxDamageRange 1000, minDamageRange 2500, adsGunKickPitchMin 7, adsGunKickPitchMax 19, adsGunKickYawMin -7, adsGunKickYawMax 13, adsGunKickAccel 1000, adsGunKickSpeedMax 2500, adsGunKickSpeedDecay 40, adsGunKickStaticDecay 50, hipViewKickPitchMin -38, hipViewKickPitchMax 75, hipViewKickYawMin 63, hipViewKickYawMax -63, hipViewKickCenterSpeed 1875, adsViewKickPitchMin -38, adsViewKickPitchMax 75, adsViewKickYawMin 63, adsViewKickYawMax -63, adsViewKickCenterSpeed 1875

**G36C Silencer**: clipSize 40, dropAmmoMax 40


**Grenade launchers**: damage 100, startAmmo 1, maxAmmo 1, explosionRadius 200, explosionInnerDamage 100, explosionOuterDamage 75


**Barett**: clipSize 1, damage 1000, minDamage 1000, startAmmo 10, maxAmmo 30, ammoCounterClip ShortMagazine


**R700**:  fireType 2-Round Burst, startAmmo 24, maxAmmo 42, clipSize 6, damage 60, minDamage 50, minDamageRange 6000, locHelmet 2, locHead 2, adsViewKickYawMin 35, adsViewKickYawMax -35


**M4**: damage 15, minDamage 10, fireTime 0.085, swayHorizScale 0.6, swayVertScale 0.15, adsSwayHorizScale 0.28, adsSwayVertScale 0.37

**gl m4**: maxAmmo 8, startAmmo 8, clipSize 8, damage 85, fireTime 0.55, reloadTime 1.3, reloadAddTime 0.75, explosionRadius 100, explosionInnerDamage 50, explosionOuterDamage 25

**M4 Base**: clipSize 25

**M4 GL**: clipSize 25

**M4 reflex**: clipSize 25

**M4 ACOG**: clipSize 25, adsSwayHorizScale 0.04, adsSwayVertScale 0.03

**M4 silencer**: damage 20, minDamage 10, maxDamageRange 500, minDamageRange 1000, fireTime 0.09, maxAmmo 100, startAmmo 60, clipSize 30, dropAmmoMax 30, reloadTime, 2.5 reloadEmptyTime 2.5, locHelmet 4, locHead 4, locRightLegUpper 0.85, locRightLegLower 0.85, locRightFoot 0.85, locLeftLegUpper 0.85, locLeftLegLower 0.85, locLeftFoot 0.85, dropTime 0.7, raiseTime 1, altDropTime 0, altRaiseTime 0.60, quickDropTime 0.5, quickRaiseTime 1, firstRaiseTime 1.1, emptyDropTime 0.7, emptyRaiseTime 1, swayHorizScale 0.3, swayVertScale 0.1, adsSwayHorizScale 0.14, adsSwayVertScale 0.25, hipSpreadStandMin 2, hipSpreadDuckedMin 1.7, hipSpreadProneMin 1.7, hipSpreadMax 9, hipSpreadDuckedMax 7.6, hipSpreadProneMax 7.6, hipSpreadFireAdd 0.6, hipSpreadMoveAdd 20, hipSpreadDecayRate 2.85, hipSpreadDuckedDecay 2.58, hipSpreadProneDecay 2.58


**M14**: damage 25, minDamage 20

**M14 ACOG**: damage 40, minDamage 30, fireTime 0.25, clipSize 10, startAmmo 20, maxAmmo 60


**M16A4**: damage 15, minDamage 10


**M16**: damage 15, minDamage 10


**M21**: damage 35, minDamage 35, fireTime 0.25


**M40A3**: moveSpeedScale 1, adsMoveSpeedScale 1, damage 70, minDamage 60, maxDamageRange 6000, minDamageRange 2000, locHelmet 2, locHead 2, locNeck 1.25, locTorsoUpper 1.25, locTorsoLower 1, locRightArmUpper 1, locLeftArmUpper 1, reloadTime 0.47666, rechamberBoltTime 0.6, raiseTime 1.5, quickRaiseTime 1, firstRaiseTime 1.5, adsSwayMaxAngle 1, adsSwayLerpSpeed 3, hipSpreadMoveAdd 3.75

**M40A3 Base**: adsViewKickPitchMin 0, adsViewKickPitchMax 30, adsViewKickYawMin 15, adsViewKickYawMax -15

**M40A3 ACOG**: hipGunKickStaticDecay 75, adsGunKickStaticDecay 75, adsViewKickPitchMin 0, adsViewKickPitchMax 10, adsViewKickYawMin 7, adsViewKickYawMax -7


**M60**: clipSize 60, damage 25, minDamage 20


**M1014 Benelli**: damage 30, minDamage 25, maxAmmo 48, startAmmo 24, clipSize8, shotCount 2, maxDamageRange 1200, minDamageRange 2400


**MP5**: damage 20, minDamage 10, swayMaxAngle 40, swayHorizScale 0.6, swayVertScale 0.2, adsSwayMaxAngle 10, adsSwayLerpSpeed 2, adsSwayPitchScale 0.2, adsSwayYawScale 0.2, adsSwayHorizScale 0.3, adsSwayVertScale 0.4

**MP5 ACOG**: adsSwayHorizScale 0.04, adsSwayVertScale 0.04


**Dragunov**: damage 30, minDamage 25, clipSize 20, startAmmo 60, maxAmmo 120, fireType 4-Round Burst, locTorsoLower 1.1, fireTime 0.1, hipGunKickYawMin -20, hipGunKickYawMax 20, hipGunKickPitchMin 40, hipGunKickPitchMax 45, adsGunKickAccel 150, adsViewKickYawMin 25, adsViewKickYawMax -30, hipViewKickPitchMin 40, hipViewKickPitchMax 60, hipViewKickYawMin 25, hipViewKickYawMax -30, adsViewKickPitchMin 10, adsViewKickPitchMax 40, adsViewKickYawMin 15 adsViewKickYawMax -20


**MP44**: damage 20, minDamage 15, fireType 2-Round Burst


**Beretta M9**: damage 20, minDamage 10, fireType 3-Round Burst, clipSize 12


**Desert Eagle**: damage 25, minDamage 15


**Desert Eagle Gold**: damage 20, minDamage 10, locHelmet 2, locHead 2.5, locNeck 2, locTorsoUpper 2


**Airstrike**: explosionRadius 300, explosionInnerDamage 150, explosionOuterDamage 25, projectileSpeed 450